<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class DefaultController extends Controller
{

    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
        ]);
    }

    public function loginAction(Request $request){
        $helpers = $this->get("app.helpers");

        $jwt_auth = $this->get("app.jwt_auth");

        $json = $request->get("json", null);



        if($json!=null){
            $params = json_decode($json); //convierte a objeto

            $email = (isset($params->email))?$params->email : null;

            $password = (isset($params->password))?$params->password : null;

            $getHash = (isset($params->gethash))?$params->gethash : null;

            $emailConstraint = new Assert\Email();

            $emailConstraint->message = "El email no es valido!!";

            $validate_email = $this->get("validator")->validate($email, $emailConstraint); //validador de symfony

            $pwd = hash('sha256',$password);

            if(count($validate_email) == 0 && $password!= null){

                if($getHash==null || $getHash=="false"){
                    //$signup = $jwt_auth->signup($email,$password, "hash"); //datos hasheados
                    $signup = $jwt_auth->signup($email,$pwd); //datos limpios

                }
                else{
                    //$signup = $jwt_auth->signup($email,$password, "hash"); //datos hasheados
                    $signup = $jwt_auth->signup($email,$pwd, true); //datos limpios

                }


                //return $helpers->json($signup);
                return new JsonResponse($signup);


            }else{
                return $helpers->json(
                    array(
                        "status" => "Error",
                        "data" => "Login not valid"
                    ));

            }

        }else{
            return $helpers->json(
                array(
                    "status" => "Error",
                    "data" => "Please send json with post"
                ));
        }

        //recibir json por post
    }


    public function pruebasAction(Request $request)
    {
        $helpers = $this->get("app.helpers");
        //$jwt_auth = $this->get("app.jwt_auth"); Se inluye en el helper

        $hash = $request->get("authorization", null);

        $check = $helpers->authCheck($hash,true);

        var_dump($check);

        die();



        $em = $this->getDoctrine()->getManager();

        $users = $em->getRepository('BackendBundle:User')->findAll();

        //$pruebas = array("id"=>1, "nombre"=>"Daniel"); //Tmb lo convierte a json lo que sea

        //echo $helpers->hola();

        return $helpers->json($users);
    }





}
