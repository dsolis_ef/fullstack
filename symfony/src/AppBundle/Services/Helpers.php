<?php
namespace AppBundle\Services;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;

/**
 * Created by PhpStorm.
 * User: danielsolis
 * Date: 19/05/17
 * Time: 17:15
 */
class Helpers
{
    public $jwt_auth;

    public function __construct($jwt_auth){

        $this->jwt_auth = $jwt_auth;

    }

    public function authCheck($hash, $getIdentity= false){

        $jwt_auth = $this->jwt_auth;

        $auth = false;

        if($hash!= null){

            if($getIdentity==false){
                $checkToken = $jwt_auth->checkToken($hash);

                if($checkToken==true){
                    $auth = true;
                }
            }else{
                $checkToken = $jwt_auth->checkToken($hash,true);
                if(is_object($checkToken)){
                    $auth = $checkToken;
                }

            }

        }

        return $auth;

    }
    public function json($data){
        $normalizers = array(new GetSetMethodNormalizer());
        $encoders = array("json"=>new JsonEncoder());

        $serializer = new Serializer($normalizers,$encoders);

        $json = $serializer->serialize($data,'json');

        $response = new Response(); //respuesta http

        $response->setContent($json);

        $response->headers->set("Content-Type","application/json");

        return $response;
    }

    public function hola(){
        return "Desde el servicio helpers";
    }
}