// Importar el núcleo de Angular
import {Component, OnInit} from '@angular/core';
import { ROUTER_DIRECTIVES, Router, ActivatedRoute } from '@angular/router';
import {LoginService} from '../services/login.service';
//import {ActivatedRoute} from "@angular/router";

// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
@Component({
    selector: 'login',
    templateUrl: 'app/view/login.html',
    providers: [LoginService]
})

// Clase del componente donde irán los datos y funcionalidades
export class LoginComponent implements OnInit{
    public titulo: string = "Identificate";
    public user;
    public errorMessage;
    public identity;
    public token;

    constructor(
        private _loginService: LoginService,
        private _route: ActivatedRoute,
        private _router: Router
    ){}

    ngOnInit(){

        this._route.params.subscribe(params => {
            let logout = params["id"];
            if(logout ==1){
                localStorage.removeItem('identity');
                localStorage.removeItem('token');
                this.identity = null;
                this.token = null;

                window.location.href = "/login";

               // this._router.navigate(["/index"]);
            }
        });

        //alert(this._loginService.signup());
        this.user = {
            "email": "",
            "password": "",
            "gethash": "false"
        };

        let ide = this._loginService.getIdentity();
        let tk =this._loginService.getToken();

        /*let ide = localStorage.getItem('identity');
        let tk = localStorage.getItem('token');*/
        /*console.log(ide);
        console.log(tk);*/
        let identity = this._loginService.getIdentity();
        if(identity != null && identity.sub){
            this._router.navigate(["/index"]);
        }

    }

    onSubmit(){
       // console.log(this.user);

        this._loginService.signup(this.user).subscribe(
            response => {

                let identity = response;

                this.identity = identity;

                //console.log(this.identity);

                if(this.identity.length <=1){
                    alert("error en el servidor");
                }else{
                    if(!this.identity.status){
                        localStorage.setItem('identity', JSON.stringify(identity));

                        //get token
                        this.user.gethash = "true";
                        //console.log(localStorage.getItem('identity'));
                        this._loginService.signup(this.user).subscribe(
                            response => {

                                let token = response;

                                this.token = token;
                                if(this.token.length <=0 ){
                                    alert("error en el servidor")
                                }else{
                                    if(!this.token.status){
                                        localStorage.setItem('token', token);

                                        //redireccion
                                        window.location.href = "/";

                                        /*let ide = localStorage.getItem('identity');
                                        let tk = localStorage.getItem('token');
                                        console.log(ide);
                                        console.log(tk);*/
                                    }
                                }

                            },
                            error => {
                                this.errorMessage = <any>error;

                                if(this.errorMessage!= null){
                                    console.log(this.errorMessage);
                                    alert("Error en la peticion");
                                }
                            }
                        );


                    }
                }

            },
            error => {
                this.errorMessage = <any>error;

                if(this.errorMessage!= null){
                    console.log(this.errorMessage);
                    alert("Error en la peticion");
                }
            }

        );

    }
}

